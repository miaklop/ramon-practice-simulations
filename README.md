# Software Needed
To run these simulations you will need the master branch of [mBuild](https://github.com/mosdef-hub/mbuild); you will want to make a software directory to store these files:


``` Bash
git clone https://github.com/mosdef-hub/mbuild.git
cd mbuild
pip install -e .
```

And to install the mbuild deps:

``` Bash
conda install --only-deps -c omnia -c mosdef mbuild
```

You will also need `cme_utils`

``` Bash
git clone git@bitbucket.org:cmelab/cme_utils.git
cd cme_utils
pip install -e .
```

You may also need

```Bash
conda install -c glotzer gsd signac
```
for cme_utils to work

# How to run simulations
Make sure you download the `moleculelib` as this contains important molecule files for some of the simulations (sims).

To run the `rigid_rings` simulations you will need to run the `moi.py` first. Make sure to update all the particle positions within the script if, these have changed. Once you have the moment of inertia (moi) valuse you will need to update these values in the `ring.py` script.

To run the `benzenesims` simulations you will first want to create the box with `box.py`. You will also need to specify the number of particles (i.e. `python box.py 75` will give a box with 75 particles.
Then you use`benzene.py` to run the simulation. You will also need to specify the temperature like the above example. Make sure that you have downloaded the `benzene.mol2` and done the steps in the "Software Needed" section. The `bezene.mol2` molecule file is located in the `moleculelib` repository. 

To run the `benzene_with_various_tails_sims` you will first want to create the box with `benzenewvarioustails.py`. You will need to specify the lenght of the tail and the number of particles you want in the sim.
Then run the sim with `tails.py`. You will also need to specify the box file and the number of repeats you want to do (i.e. `python tails.py bt0_box.hoomdxml 2`; this will repeat the simulation twice and since the sim is set to a temperature of 0.5 it will do the first run at 0.5 and then do the second run at 1.0 as 0.5 is added to the temperature after each run ). All the box files are named like the one in the example except the number changes depending on the length of the chain. Make sure you have the `benzene.mol2` and `carbon.mol2` molecule files located in `moleculelib`.

To check the equilibration of the sims use `plot.py`. You will also need to specify the log file. This will then return the potential energy (PE) and temperature versus the timestep. (Useful tool :|)

If you have any questions please feel free to contact me in person or email me at rm2258@nau.edu.